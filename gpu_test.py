from tensorflow.python.client import device_lib
import tensorflow as tf
from os import environ

if __name__ == '__main__':
    environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
    local_device_protos = device_lib.list_local_devices()
    print([x.name for x in local_device_protos if x.device_type == 'GPU'])
