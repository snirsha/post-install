#!/usr/bin/env bash

if [[ "$EUID" -ne 0 ]];then
  echo "Please run as root Or run 'sudo bash $0'"
  exit 0
fi
test_count=0
tests=()
if [[ $(nvidia-smi --query-gpu=count --format=csv,noheader) -gt 0 ]];then
  ((test_count=test_count+1))
else
  tests+=( "nvidia-smi" )
fi

sudo apt install ./libcudnn7-doc_7.6.5.32-1+cuda10.0_amd64.deb -q -y >/dev/null 2>&1
cd /usr/src/cudnn_samples_v7/mnistCUDNN/ || exit
sudo make clean --quiet && sudo make --quiet -j "$(nproc --all)"
if [[ $(./mnistCUDNN | grep -c "Test passed!") -eq 2 ]];then
  ((test_count=test_count+1))
else
  tests+=( "cudnn and cuda-toolkit" )
fi

cd - || exit

sudo apt install -q -y python3-virtualenv virtualenv >/dev/null 2>&1
virtualenv test_tensorflow --python=python3.7 -q
source test_tensorflow/bin/activate
if ! pip list | grep tensorflow-gpu  >/dev/null 2>&1;then 
  pip install tensorflow-gpu==1.14.0
  sed -i "28i np.warnings.filterwarnings('ignore')" test_tensorflow/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py
fi 
if [[ -n $(python ./gpu_test.py 2> /dev/null) ]]; then
  ((test_count=test_count+1))
else 
  tests+=( "tensorflow-gpu" )
fi

deactivate
echo "${test_count} out of 3"
if [[ ${test_count} -lt 3 ]]; then 
  echo "Failed Tests: ${tests[@]}"
fi
chown -R 1000:1000 ~